
WiSHFUL Framework
=================

This repo contains the code for a LoRa mote controller using the Unified
Programming Interfaces (UPIs) of the WiSHFUL software platform for radio and
network control. 




## Acknowledgement

The research leading to these results has received funding from the European
Horizon 2020 Programme under grant agreement 645274 (WiSHFUL project). 
