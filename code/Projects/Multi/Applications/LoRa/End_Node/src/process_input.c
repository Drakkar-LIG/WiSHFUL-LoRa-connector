/*
 * process_input.c
 *
 *  Created on: Mar 12, 2018
 *      Author: qasimlone
 */

#include "process_input.h"
#include <string.h>

/* Private define ------------------------------------------------------------*/
#define PARM_SIZE 128
#define CMD_SIZE 7
char char_buff[7];

extern char rxData[40];

/*
struct tx_variables{
	int tx_dr; //data rate
	int cr; // coding rate
	int tx_pwr; //transmission power
	long freq; //frequency
}parm;
*/


/* recieve data from serial port
 * and parse it to setup radio layer
 */
int hex_to_int(char hex) {
	if (hex > '9')
		// uppercase letter
		return (hex - 'A') + 10;
	else
		// uppercase letter
		return hex - '0';
}
void Serial_Data()
{

	//PRINTF("In serial data\n");
	//HAL_Delay(500);
	if (IsNewCharReceived() == SET){
		PRINTF("In serial data\n");
	 char recv_data = GetNewChar();
	PRINTF("%c\n", recv_data);}

}
void GetData_serial(void)
{

	 static char command[CMD_SIZE];
	  static unsigned i = 0;
	  unsigned cmd_size = 0;
	  PRINTF("IN Get data");

	  /* Process all commands */
	  while (IsNewCharReceived() == SET)
	  {
	    command[i] = GetNewChar();

	    PRINTF("char recv. %c", command[i]);



	    if ((command[i] == '\r') || (command[i] == '\n'))
	    {
	      if (i != 0)
	      {
	        command[i] = '\0';
	        /* need to put static i=0 to avoid realtime issue when CR+LF is used by hyperterminal */
	        cmd_size = i;
	        i = 0;
	        PRINTF("IN Get data");
	        PRINTF(command);
	       // parse_cmd(command);

	        memset(command, '\0', cmd_size);

	      }
	    }
	    else
	    if (i == (CMD_SIZE - 1))
	    {
	      memset(command, '\0', i);
	      i = 0;

	    }
	    else
	    {
	      i++;
	    }
	  }


}

/**
  * @brief This function processes the input sent from serial comm port
  * @param None
  * @retval None
  */

void Process_Commands(void)
{

	//rxData[6];
//	PRINTF("char rec %c and size %d\r\n",rxData[17],strlen(rxData) );
	// Parse data
	char dest[50];
	memcpy(dest,rxData, strlen(rxData)+1);
    //PRINTF("char_are %s,%c,%c,%c,%c\r\n",rxData,dest[0],dest[6],dest[19],dest[35]);
	    	//PRINTF("%s\n",char_buff);
    // To allign data it should start with ^ and end with a $
    if  (  (dest[0]  == '^') && (dest[5]  == '$') )
    {
    	char cmd[3];
    	strncpy(cmd, dest+1, 2);
    	cmd[2] = '\0';

    	//PRINTF("Command is %s\r\n", cmd);

    	int num = (dest[3]-'0')*10 + (dest[4]-'0');
    	//PRINTF("Number is %d\r\n",num);

    	if (strcmp(cmd,"ad") == 0)
		{

			PRINTF("Setting ADR %d\r\n",num);
			setAdr(num);
			//PRINTF("%s_%s\n",cmd,num);
			//char_buff[0] = '\0';

		}

    	if (strcmp(cmd,"dr") == 0)
    	{

    		PRINTF("Setting data rate %d\r\n",num);
    		setDataRate(num);
    		//PRINTF("%s_%s\n",cmd,num);
    		//char_buff[0] = '\0';

    	}
    	else if (strcmp(cmd,"tx") == 0)
    	{

    		PRINTF("Setting up tx power %d\r\n", num);
    		setTxPower(num);
    		//PRINTF("%s_%s\n",cmd,num);

    	}
    	else if  (strcmp(cmd,"cr") == 0)
    	{
    		//Get value
    		//0 is 4/5 1 is 4/6 2 is 4/7 and 3 is 4/8

    		PRINTF("Setting coding rate %d\r\n",num);
    		setCodeRate(num);


    		//PRINTF("Setting Channel %.3f", ret);

        }
    	//Get values
    	else if  (strcmp(cmd,"gt") == 0)
    	{

    		if (num ==0)
    		{
    			int res=0;
    			res =getNetworkStatus();
    			PRINTF("%d\r\n",res);
    		}

    		if (num ==1)
			{
				int res=0;
				res =getTxPower();
				PRINTF("%d\r\n",res);
			}

    		if (num ==2)
			{
				int res=0;
				res =getDataRate();
				PRINTF("%d\r\n",res);
			}
    		if (num == 3)
    		{
    			getDevEui();
    		}
    		if (num == 4)
			{
				getAppEui();
			}
    		if (num == 5)
			{
				getAppkey();
			}



    	}

	  }
	else if ( (dest[0]  == '^') && (dest[19]  == '$'))
	{
	    char cmd[3];
	    strncpy(cmd, dest+1, 2);
	    cmd[2] = '\0';
	    char msg[17];
	    strncpy(msg, dest+3, 16);
	    msg[16] = '\0';
	    uint8_t hex[16];

	    //PRINTF("EUI is %s\r\n",msg);

	    for (int i=0; i<16; i++)
	    {

	      hex[i] = hex_to_int(*(msg+2*i))*16 + hex_to_int(*(msg+2*i+1));
	        		//	PRINTF("Char are %c and %c\n", *(msg+2*i), *(msg+2*i+1));
	     }
	    if  (strcmp(cmd,"eu") == 0){
	    	PRINTF("Setting device EUI  %s\r\n",msg);
	    	setDevEui(hex);
	    }

	    if  (strcmp(cmd,"ae") == 0){

	    	setAppEui(hex);
	    	PRINTF("Setting device App EUI  %s\r\n",msg);
	    }

	}
	else if ( (dest[0]  == '^') && (dest[35]  == '$'))
		{
		    char cmd[3];
		    strncpy(cmd, dest+1, 2);
		    cmd[2] = '\0';
		    char msg[33];
		    strncpy(msg, dest+3, 32);
		    msg[32] = '\0';
		    uint8_t hex[32];

		    //PRINTF("EUI is %s\r\n",msg);

		    for (int i=0; i<32; i++)
		    {

		      hex[i] = hex_to_int(*(msg+2*i))*16 + hex_to_int(*(msg+2*i+1));
		        		//	PRINTF("Char are %c and %c\n", *(msg+2*i), *(msg+2*i+1));
		     }

		    if  (strcmp(cmd,"ak") == 0){

		    	setDevAK(hex);
		    	PRINTF("Setting device App key  %s\r\n",msg);
		    }

		}



    else
    {

    	PRINTF("0\r\n");

    	//PRINTF("Wrong data %s\r\n",dest);
    /*	char cmd[2];

    	    	//char *cmd;
    			char args_[2],rest[2];
    	    	sscanf(dest,"%[^_]_%[^_]%s",cmd,args_,rest);
    	    	char *ptr;
    	    	long ret;

    	    	//char *cmd;
    	    	//char *saveptr;
    	    	//char *foo, *bar;

    	    	//foo = strtok_r(str, " ", &saveptr);
    	    //	bar = strtok_r(NULL, " ", &saveptr);

    	    	//cmd = strtok(dest, "_");

    	    	char *token, *cp;
    	    	//memcpy(out,in + start_offset, total_len - end -start_offset);
    	    	memcpy(cmd,dest+1, 	2*sizeof(char));



    	    	PRINTF("Command is %s\n", cmd);*/


    }




}








void GetParameters_serial(void)
{

    //int tick = HAL_GetTick();
    Get_Data();
    //Get_Data_blocking();
    HAL_Delay(400);
     // PRINTF("rec:%s\n",char_buff);
    // check if we have incomming data
    if ((char_buff != NULL) && (char_buff[0] != '\0')) {

    	//PRINTF("Wrong data %s\n",char_buff);
    	PRINTF("char_are %s,%s,%s\n",char_buff,char_buff[0],char_buff[6]);


    	//strcmp(items[n], "ae") == 0
    	//while ((((char*)char_buff[0] ) != '^') && (((char*)char_buff[6] ) != '$') )
    	while (  (char_buff[0]  != '^') && (char_buff[6]  != '$') )
    	{
    		//Get_Data_blocking();




    		Get_Data();
    		HAL_Delay(400);
    		PRINTF("Wrong data %s\n",char_buff);
    		// (*ptr)[0]
    		PRINTF("char_are %s,%c,%c,\n",char_buff,(char*)char_buff[0],(char*)char_buff[6]);
    		// memset(char_buff, 0, 7);


    	}





    	char cmd[2],args_[2],rest[2];
    	sscanf(char_buff,"%[^_]_%[^_]%s",cmd,args_,rest);
    	char *ptr;
    	long ret;
    	ret = strtol(args_, &ptr, 10);



    	if (strcmp(cmd,"^dr") == 0)
    	{
    		PRINTF("Setting data rate %d\n",ret);
    		setDataRate(ret);
    		PRINTF("%s_%s\n",cmd,args_);
    		char_buff[0] = '\0';
    		memset(char_buff, 0, 7);
    	}





    }

}
