/*
 * proccess_input.h
 *
 *  Created on: Mar 12, 2018
 *      Author: qasimlone
 */

#ifndef PROJECTS_END_NODE_PROCCESS_INPUT_H_
#define PROJECTS_END_NODE_PROCCESS_INPUT_H_



#endif /* PROJECTS_END_NODE_PROCCESS_INPUT_H_ */

/* Includes ------------------------------------------------------------------*/
#include "vcom.h"
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* External variables --------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */

/**
* @brief  Read input from serial.
* @param  None
* @return None
*/

void GetParameters_serial(void);
